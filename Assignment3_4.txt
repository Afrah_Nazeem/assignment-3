//Program to find factors of given number
#include <stdio.h>

int main(){
int number=0;
printf("Enter a Number : ");
scanf("%d",&number);

int i=1;
while(i<=number){
if(number%i==0) printf("%d \n",i);
i++;
}
printf("\n");
return 0;
}
